<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://techmirza.com
 * @since      1.0.0
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $tab = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : "instructions";
?>

<div class="wrap lovely-users-settings-div">
    <h2><?php _e( 'Lovely Users Settings', 'lovely-users' ); ?></h2>
    <?php settings_errors(); ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=lovely-users-settings&tab=instructions'); ?>" class="nav-tab <?php echo ($tab=='instructions') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Instructions', 'lovely-users' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=lovely-users-settings&tab=settings-menu'); ?>" class="nav-tab <?php echo ($tab=='settings-menu') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Settings Menu', 'lovely-users' ); ?>
        </a>
    </h2>
    <?php if ($tab == 'instructions') { ?>
        <ul>
            <li><?php _e( 'Following is the list of custom actions that are available under this plugin.'); ?></li>
            <li><?php _e( "1. <code>do_action('lovely_users_before_content')</code> is the action to add content before <strong>Loverly Users Content</strong>.", 'lovely-users' ); ?></li>
            <li><?php _e( "2. <code>do_action('lovely_users_after_content')</code> is the action to add content after <strong>Loverly Users Content</strong>.", 'lovely-users' ); ?></li>
            <li><?php _e(" 3. If you're going to overriding the template. Make sure you're creating new template file in your current theme root directory. After creating the file, please copy the contents of template from 'wp-content/plugins/lovely-users/src/frontend/partials/lovely-users-public-display.php' to newly created template file. You're ready to go on just make changes and insert appropriate name of new file into the settings template file name.", 'lovely-users' ); ?></li>
        </ul>
    <?php } ?>
    <?php if ($tab == 'settings-menu') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'lovely-users-settings' );
            do_settings_sections( 'lovely-users-settings' );
            $lovelyUsersSettings = get_option('lovely_users_settings', array()); ?>
            <table class="widefat form-table lovely-users-setting-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="template_file"><?php _e( 'Template File Name', 'lovely-users' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="lovely_users_settings[template_file]" id="template_file" class="wd100" value="<?php echo (isset($lovelyUsersSettings['template_file']) && !empty($lovelyUsersSettings['template_file'])) ? $lovelyUsersSettings['template_file'] : 'my-lovely-users-table.php'; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="endpoint"><?php _e( 'Lovely Users Endpoint', 'lovely-users' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="lovely_users_settings[endpoint]" id="endpoint" class="wd100" value="<?php echo (isset($lovelyUsersSettings['endpoint']) && !empty($lovelyUsersSettings['endpoint'])) ? $lovelyUsersSettings['endpoint'] : 'my-lovely-users-table'; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
</div>