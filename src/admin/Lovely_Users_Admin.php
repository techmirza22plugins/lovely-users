<?php

namespace Admin;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://techmirza.com
 * @since      1.0.0
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/admin
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Lovely_Users_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $client;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Create default HandlerStack
		$stack = HandlerStack::create();
		// Add this middleware to the top with `push`
		$stack->push(new CacheMiddleware(), 'cache');
		// Initialize the client with the handler option
		$this->$client = new Client(['handler' => $stack, 'http_errors' => false]);

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lovely_Users_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lovely_Users_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/lovely-users-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lovely_Users_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lovely_Users_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/lovely-users-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function lovely_users_settings() {
		register_setting( 'lovely-users-settings', 'lovely_users_settings' );
		register_setting( 'lovely-users-general-settings', 'lovely_users_general_settings' );
	}

	public function lovely_users_menu() {
		add_menu_page(
			'Lovely Users Settings',
			'Lovely Users Settings',
			'manage_options',
			'lovely-users-settings',
			array($this, 'plugin_settings_callback')
		);
	}

	public function plugin_settings_callback() {
		require('partials/lovely-users-admin-display.php');
	}

	public function get_user_details() {
		$user_id = (int) $_POST['user_id'];
		$albums = $this->get_lovely_user_details('https://jsonplaceholder.typicode.com/users/'.$user_id.'/albums');
		if (gettype($albums) === 'object') {
			$albums = json_decode($albums);
		} else {
			$albums = json_decode ('{}');
		}
		$todos = $this->get_lovely_user_details('https://jsonplaceholder.typicode.com/users/'.$user_id.'/todos');
		if (gettype($todos) === 'object') {
			$todos = json_decode($todos);
		} else {
			$todos = json_decode ('{}');
		}
		$posts = $this->get_lovely_user_details('https://jsonplaceholder.typicode.com/users/'.$user_id.'/posts');
		if (gettype($posts) === 'object') {
			$posts = json_decode($posts);
		} else {
			$posts = json_decode ('{}');
		}
		$response = array();
		$response['albums'] = $albums;
		$response['todos'] = $todos;
		$response['posts'] = $posts;
		echo json_encode($response);
		exit();
	}

	private function get_lovely_user_details($url_string) {
		$response = $this->$client->request('GET', $url_string);
		if ($response->getStatusCode() === 200) {
			return $response->getBody();
		} else {
			return false;
		}
	}

}
