<?php

namespace Frontend;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://techmirza.com
 * @since      1.0.0
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/public
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Lovely_Users_Frontend {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $client;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Create default HandlerStack
		$stack = HandlerStack::create();
		// Add this middleware to the top with `push`
		$stack->push(new CacheMiddleware(), 'cache');
		// Initialize the client with the handler option
		$this->$client = new Client(['handler' => $stack, 'http_errors' => false]);

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lovely_Users_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lovely_Users_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-dataTables-bootstrap5-css', plugin_dir_url( __FILE__ ) . 'css/dataTables.bootstrap5.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/lovely-users-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lovely_Users_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lovely_Users_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.bundle.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery.dataTables-js', plugin_dir_url( __FILE__ ) . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-dataTables-bootstrap5-js', plugin_dir_url( __FILE__ ) . 'js/dataTables.bootstrap5.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery-loading-js', plugin_dir_url( __FILE__ ) . 'js/jquery.loading.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/lovely-users-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	public function load_custom_template( $template ) {
		// Getting Website URL
		$websiteURL = get_bloginfo( 'wpurl' );
		// Getting Current URL using parse_url()
		$currentURL = parse_url( home_url() );
		$currentURL = "{$currentURL['scheme']}://{$currentURL['host']}" . add_query_arg( NULL, NULL );
		$lovelyUsersSettings = get_option('lovely_users_settings', array());
		$endpoint = 'my-lovely-users-table';
		if (isset($lovelyUsersSettings['endpoint']) && !empty($lovelyUsersSettings['endpoint'])) {
			$endpoint = $lovelyUsersSettings['endpoint'];
		}
		// Trigger if the current url matches the defined one
		if ( $websiteURL . '/' . $endpoint === $currentURL ) {
			$file_name = '';
			if (isset($lovelyUsersSettings['template_file']) && !empty($lovelyUsersSettings['template_file'])) {
				$file_name = $lovelyUsersSettings['template_file'];
			}
			// Fetching users from API
			$users = $this->get_lovely_users('https://jsonplaceholder.typicode.com/users');
			if (gettype($users) === 'object') {
				$users = json_decode($users);
			} else {
				$users = json_decode ('{}');
			}
			if ( locate_template($file_name) ) {
				// Template found in theme's folder
				include_once(locate_template($file_name));
				exit();
			} else {
				// Template not found in theme's folder, use plugin's template as a fallback
				require_once dirname( __FILE__ ) . '/partials/lovely-users-public-display.php';
				exit();
			}
		} else {
			return $template;
		}
	}

	private function get_lovely_users($url_string) {
		$response = $this->$client->request('GET', $url_string);
		if ($response->getStatusCode() === 200) {
			return $response->getBody();
		} else {
			return false;
		}
	}

}
