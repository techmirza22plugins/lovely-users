(function( $ ) {
	
	$(document).ready(function() {
		// Initializing Datatables
		if ($(document).find(".lovely-users-table").length > 0) {
			$(document).find(".lovely-users-table").DataTable();
		}
		// Function to get user post details
		$(document).on("click", ".user-details-link", function() {
			$(document).find(".user-details-container").loading("show");
			$('html, body').animate({
				scrollTop: $(document).find(".user-details-container").offset().top
			}, 1500);
			var user_id = parseInt($(this).data("user-id"));
			var name = $(this).data("user-name");
			if (user_id && user_id > 0) {
				var data = {
					'action': 'get_user_details',
					'user_id': user_id
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					var response = JSON.parse(response);
					var albums = response.albums;
					var todos = response.todos;
					var posts = response.posts;
					$(document).find(".user-details-container span.name").text(name);
					if ($.fn.DataTable.isDataTable(".lovely-user-albums-table")) {
						$(document).find(".lovely-user-albums-table").dataTable().fnClearTable();
						$(document).find(".lovely-user-albums-table").dataTable().fnDestroy();
					}
					if ($.fn.DataTable.isDataTable(".lovely-user-todos-table")) {
						$(document).find(".lovely-user-todos-table").dataTable().fnClearTable();
						$(document).find(".lovely-user-todos-table").dataTable().fnDestroy();
					}
					if ($.fn.DataTable.isDataTable(".lovely-user-posts-table")) {
						$(document).find(".lovely-user-posts-table").dataTable().fnClearTable();
						$(document).find(".lovely-user-posts-table").dataTable().fnDestroy();
					}
					if (albums.length > 0) {
						albums.forEach(element => {
							var html = '<tr>';
							html += '<td>'+element.id+'</td>';
							html += '<td>'+element.title+'</td>';
							html += '</tr>';
							$(document).find(".lovely-user-albums-table tbody").append(html);
						});
					}
					$(document).find(".lovely-user-albums-table").DataTable({
						"columnDefs": [
							{ "width": "10%", "targets": 0 },
							{ "width": "90%", "targets": 1 },
						]
					});
					if (todos.length > 0) {
						todos.forEach(element => {
							var html = '<tr>';
							html += '<td>'+element.id+'</td>';
							html += '<td>'+element.title+'</td>';
							if (element.completed) {
								html += '<td><span class="badge bg-success">Completed</span></td>';
							} else {
								html += '<td><span class="badge bg-danger">Incomplete</span></td>';
							}
							html += '</tr>';
							$(document).find(".lovely-user-todos-table tbody").append(html);
						});
					}
					$(document).find(".lovely-user-todos-table").DataTable({
						"columnDefs": [
							{ "width": "10%", "targets": 0 },
							{ "width": "80%", "targets": 1 },
							{ "width": "10%", "targets": 2 },
						]
					});
					if (posts.length > 0) {
						posts.forEach(element => {
							var html = '<tr>';
							html += '<td>'+element.id+'</td>';
							html += '<td>'+element.title+'</td>';
							html += '<td>'+element.body+'</td>';
							html += '</tr>';
							$(document).find(".lovely-user-posts-table tbody").append(html);
						});
					}
					$(document).find(".lovely-user-posts-table").DataTable({
						"columnDefs": [
							{ "width": "10%", "targets": 0 },
							{ "width": "40%", "targets": 1 },
							{ "width": "50%", "targets": 2 },
						]
					});
					$(document).find(".user-details-container").loading("stop");
				});
			} else {
				$(document).find(".user-details-container span.name").text(name);
				if ($.fn.DataTable.isDataTable(".lovely-user-albums-table")) {
					$(document).find(".lovely-user-albums-table").dataTable().fnClearTable();
					$(document).find(".lovely-user-albums-table").dataTable().fnDestroy();
				}
				$(document).find(".lovely-user-albums-table").DataTable({
					"columnDefs": [
						{ "width": "10%", "targets": 0 },
						{ "width": "90%", "targets": 1 },
					]
				});
				if ($.fn.DataTable.isDataTable(".lovely-user-todos-table")) {
					$(document).find(".lovely-user-todos-table").dataTable().fnClearTable();
					$(document).find(".lovely-user-todos-table").dataTable().fnDestroy();
				}
				$(document).find(".lovely-user-todos-table").DataTable({
					"columnDefs": [
						{ "width": "10%", "targets": 0 },
						{ "width": "80%", "targets": 1 },
						{ "width": "10%", "targets": 2 },
					]
				});
				if ($.fn.DataTable.isDataTable(".lovely-user-posts-table")) {
					$(document).find(".lovely-user-posts-table").dataTable().fnClearTable();
					$(document).find(".lovely-user-posts-table").dataTable().fnDestroy();
				}
				$(document).find(".lovely-user-posts-table").DataTable({
					"columnDefs": [
						{ "width": "10%", "targets": 0 },
						{ "width": "40%", "targets": 1 },
						{ "width": "50%", "targets": 2 },
					]
				});
				console.log("User ID doesn't exist.");
				$(document).find(".user-details-container").loading("stop");
			}
			return false;
		});
	});

})( jQuery );
