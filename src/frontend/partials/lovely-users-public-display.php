<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://techmirza.com
 * @since      1.0.0
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php echo get_header(); ?>
<div class="container lovely-users-container">
    <?php do_action ( 'lovely_users_before_content' ); ?>
    <h2><?php _e('Users Table', 'lovely-users'); ?></h2>
    <table class="table table-striped lovely-users-table" style="width:100%">
        <thead>
            <tr>
                <th><?php _e('ID', 'lovely-users'); ?></th>
                <th><?php _e('Name', 'lovely-users'); ?></th>
                <th><?php _e('Username', 'lovely-users'); ?></th>
                <th><?php _e('Email', 'lovely-users'); ?></th>
                <th><?php _e('Phone', 'lovely-users'); ?></th>
                <th><?php _e('Website', 'lovely-users'); ?></th>
                <th><?php _e('Company', 'lovely-users'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($users) && is_array($users) && count($users) > 0) { ?>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><a href="#" class="user-details-link" data-user-id="<?php echo $user->id; ?>" data-user-name="<?php echo $user->name; ?>"><?php echo $user->id; ?></a></td>
                        <td><a href="#" class="user-details-link" data-user-id="<?php echo $user->id; ?>" data-user-name="<?php echo $user->name; ?>"><?php echo $user->name; ?></a></td>
                        <td><a href="#" class="user-details-link" data-user-id="<?php echo $user->id; ?>" data-user-name="<?php echo $user->name; ?>"><?php echo $user->username; ?></a></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->phone; ?></td>
                        <td><?php echo $user->website; ?></td>
                        <td><?php echo $user->company->name; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th><?php _e('ID', 'lovely-users'); ?></th>
                <th><?php _e('Name', 'lovely-users'); ?></th>
                <th><?php _e('Username', 'lovely-users'); ?></th>
                <th><?php _e('Email', 'lovely-users'); ?></th>
                <th><?php _e('Phone', 'lovely-users'); ?></th>
                <th><?php _e('Website', 'lovely-users'); ?></th>
                <th><?php _e('Company', 'lovely-users'); ?></th>
            </tr>
        </tfoot>
    </table>
    <div class="user-details-container">
        <h3><span class="name">User</span>'s Details</h3>
        <h4>Albums</h4>
        <table class="table table-striped lovely-user-albums-table" data-title="albums" style="width:100%">
            <thead>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <h4>Todos</h4>
        <table class="table table-striped lovely-user-todos-table" data-title="todos" style="width:100%">
            <thead>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                    <th><?php _e('Completed', 'lovely-users'); ?></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                    <th><?php _e('Status', 'lovely-users'); ?></th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <h4>Posts</h4>
        <table class="table table-striped lovely-user-posts-table" data-title="posts" style="width:100%">
            <thead>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                    <th><?php _e('Body', 'lovely-users'); ?></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th><?php _e('ID', 'lovely-users'); ?></th>
                    <th><?php _e('Title', 'lovely-users'); ?></th>
                    <th><?php _e('Body', 'lovely-users'); ?></th>
                </tr>
            </tfoot>
        </table>
        <hr>
    </div>
    <?php do_action ( 'lovely_users_after_content' ); ?>
</div>
<?php echo get_footer(); ?>