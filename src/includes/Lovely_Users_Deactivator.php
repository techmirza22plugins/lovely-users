<?php

namespace Includes;

/**
 * Fired during plugin deactivation
 *
 * @link       https://techmirza.com
 * @since      1.0.0
 *
 * @package    Lovely_Users
 * @subpackage Lovely_Users/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Lovely_Users
 * @subpackage Lovely_Users/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Lovely_Users_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
