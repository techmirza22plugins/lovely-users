<?php

namespace LovelyUsersPlugin;

use Includes\Lovely_Users_Activator;
use Includes\Lovely_Users_Deactivator;
use Includes\Lovely_Users_Core;

class Lovely_Users
{
    public function __construct($plugin_file) {
        register_activation_hook( $plugin_file, array( $this, 'activate_lovely_users' ) );
        register_deactivation_hook( $plugin_file, array( $this, 'deactivate_lovely_users' ) );
        $this->run_lovely_users();
    }

    /**
     * The code that runs during plugin activation.
     */
    public function activate_lovely_users() {
        Lovely_Users_Activator::activate();
    }

    /**
     * The code that runs during plugin deactivation.
     */
    public function deactivate_lovely_users() {
        Lovely_Users_Deactivator::deactivate();
    }

    /**
     * Begins execution of the plugin.
     *
     * Since everything within the plugin is registered via hooks,
     * then kicking off the plugin from this point in the file does
     * not affect the page life cycle.
     *
     * @since    1.0.0
     */
    public function run_lovely_users() {
        $plugin = new Lovely_Users_Core();
        $plugin->run();
    }
}