<?php

use GuzzleHttp\Client;

class LovelyUsersAPITest extends \PHPUnit\Framework\TestCase {

    public function test_getAllLovelyUsers() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_getAllLovelyUsersIsJSONType() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users');
        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json; charset=utf-8", $contentType);
    }

    public function test_getAllAlbumsOfUser() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/albums');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_getAllAlbumsOfUserIsJSONType() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/albums');
        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json; charset=utf-8", $contentType);
    }

    public function test_getAllPostsOfUser() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/posts');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_getAllPostsOfUserIsJSONType() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/posts');
        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json; charset=utf-8", $contentType);
    }

    public function test_getAllTodosOfUser() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/todos');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_getAllTodosOfUserIsJSONType() {
        $client = new GuzzleHttp\Client();
        // $this->assertEquals(true, true);
        $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/users/1/todos');
        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json; charset=utf-8", $contentType);
    }

}