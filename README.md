# Lovely Users (A Wordpress Plugin)
**This plugin provides functionality to fetch users from a third party API source and list all of them in a table with their details.**
#### List of Requirements
1. Write a WordPress plugin that will fetch the users from API and show them in a table.
2. Composer.json is valid and complete, and running composer install makes the plugin ready to be used.
3. At frontend, there will be a table to show the users with links to some of the columns. By clicking on the links, the details of specific user will be displayed without reloading the page.
4. At backend, there is no need to store data of users into the database. The fetching logic must be in backend because this work will be evaluated on the basis of backend quality of work.
5. Moreover, the table will be displayed at custom endpoint. With “custom endpoint” we mean an arbitrary URL that WP does not recognize as a standard URL, like a post permalink or so.
6. As long as fetching data through API is concerned. https://jsonplaceholder.typicode.com/ should be used for API references.
7. Frontend technologies should be around JavaScript. Moreover JQuery, TypeScript and React etc. can also be used in this project.
8. For cache, please do focus on backend HTTP requests instead of using cache techniques at frontend.
9. Proper error handling should be focused.
10. System requirements include to target PHP version 7.2 or higher and latest WordPress version.
11. At last, unit tests should be provided in order to test the functionality of the plugin.

### Installation and Usage Instructions
**Following steps are required to be focused while installtion of this plugin in your wordpress repository.**
1. You will be provided a zip file including all the plugin files in it.
2. If you're going to upload the zip file to your WordPress through wp-admin, then move to your dashbaord and go to Plugins -> Add New Plugin and then click on Upload Plugin. After selecting the zip file, the plugin will be uploaded to your WordPress. Before activation, please open the command promp and do a composer update to get the vendor directory in your plugin.
3. If you're going to extract the plugin and move it's files under wp-content/plugins directory. Just unzip the zip file and move the folder to wp-content/plugins folder. Now enter into that folder and do composer update to install the dependencies of the plugin.
4. Now the plugin is ready to use. Just activate the plugin from dashboard.
5. After activating the plugin you will see the admin menu appears in the left side of admin panel named as "Lovely Users Settings", means that you've successfully installed the plugins.

**Following steps are required to be focused while installtion of this plugin in your wordpress repository.**
1. If you've plugin activated in your WordPress repository. You will have a new admin menu in the left side of admin panel. Please click on that to view plugin settings page.
2. At there, there will be two tabs. The first tab will be about instructions and the second tab is about settings of some variable that the plugin will use.
3. At Instructions tab, there will be explanation of some actions I have added in the plugin template file. Moreover, there will be explanation about if you use the theme file then how can you do that. Well, you can make your own template there. For that, you need to copy the template that is using by the plugin which is located under lovely-users/src/frontend/partials/ named as "lovely-users-public-display.php" and make the changes in this newly created file. Make sure that you have created the new template file in your current theme root directory. 
4. At Settings tab, there will be some setting fields which are being used by the plugin. Now at here there are two different things.
    1. **Template File Name:** It is basically file name that you've created in your current theme directory. Make sure that it will be exactly same file name which you're going to use as template file. The file name should be complete. For example lovely-users-table.php. (With extension)
    2. **Endpoint URL:** It defines the endpoint url at which you want to show the users table on your WordPress website. Make sure it is without slash character (/).
    3. Well, I have entered the placeholders in these fields so that it helps you to understand how to put values there.


### Packages used in Package.JSON
1. PHP Package (To confirm that this plugin will require atleast PHP version 7.0.)
2. Guzzle HTTP Package (I have used this package to create HTTP requests for fetching the data through third party API.)
3. Guzzle Cache Middleware Package (I have used this package to include the cache in HTTP requests in order to keep server usage low.)
4. PHP Unit Package (To run the unit tests.)
5. WP CLI Package (I haven't used this package before. I just tried this package for the first time to create the unit tests class automatically and extend it to WP_UnitTestCase class to have the features of factory etc.)
6. WP CLI (Scaffold Command) Package (I haven't used this package before. I just tried this package for the first time but I found some issues in using it. Actually, while configuring it some of the files are not downloaded due to inappropriate commands for windows.)
7. Brain Monkey Package (I haven't used this package before. But as per your recommendation, I have tried this package to generate unit tests through Brain Monkey. But, I have not created any tests through it because I will need some more time to learn it.)
8. PHP Codesniffer Package (I have used this package to check that the code should be clean and simple).
9. WP Coding Standards Package (I have used this package to maintain the WordPress coding standards as provided by WordPress developer documentation.)
10. PSR-4 (This is used for coding standards for PHP's autoloaders.)

### Automated Unit Tests
Some tests are provided in the plugin folder. You can find the tests under tests folder in the plugin root directory. For performing the tests, make sure that you've opened the CMD in root directory of the plugin and then run the command "vendor/bin/phpunit".

### Demonstration Vedio
[Demonstration Video Link](https://drive.google.com/file/d/1JvnsHWDcw8BzlR8cSwmZKgUU3MJ7iTK-/view)


