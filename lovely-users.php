<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://techmirza.com
 * @since             1.0.0
 * @package           Lovely_Users
 *
 * @wordpress-plugin
 * Plugin Name:       Lovely Users
 * Plugin URI:        https://techmirza.com
 * Description:       A simple plugin to fetch all users and to display them in a table with details.
 * Version:           1.0.0
 * Author:            Tech Mirza
 * Author URI:        https://techmirza.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lovely-users
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'LOVELY_USERS_VERSION', '1.0.0' );
define( 'LOVELY_USERS_PATH', plugin_dir_path(__FILE__) );
define( 'LOVELY_USERS_URL', plugin_dir_url(__FILE__) );

require __DIR__ . '/vendor/autoload.php';

use LovelyUsersPlugin\Lovely_Users;
new Lovely_Users(__FILE__);
